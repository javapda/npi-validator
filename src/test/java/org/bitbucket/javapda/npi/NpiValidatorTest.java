package org.bitbucket.javapda.npi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class NpiValidatorTest {
    String[] validNpis = { 
        "1234567893", 
        "1356585673" // yes valid
    };
    String[] invalidNpis = { 
        "1740243034", // not valid
        "1324",
        "1ABc243034", };

    @Test
    public void testingValidNpis() {
        List<String> npis = Arrays.asList(validNpis);
        for (String npi : npis) {
            assertTrue(new NpiValidator(npi).isValid());
        }
    }

    @Test
    public void testingInvalidNpis() {
        List<String> npis = Arrays.asList(invalidNpis);
        for (String npi : npis) {
            assertFalse(new NpiValidator(npi).isValid());
        }
    }

    @Test
    public void testRegex() {
        assertTrue("1234567890".matches("[0-9]{10}"));

        assertFalse("123456789".matches("[0-9]{10}"));
        assertFalse("".matches("[0-9]{10}"));
        assertFalse("A234567890".matches("[0-9]{10}"));
        assertFalse("12A4567890".matches("[0-9]{10}"));
    }
}