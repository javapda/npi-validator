package org.bitbucket.javapda.npi;

import java.util.ArrayList;
import java.util.List;

public class NpiValidator {

    private String npi;

    public NpiValidator(String npi) {
        this.npi = npi.trim();
    }

    public boolean isValid() {
        return npi.length() == 10 && complies();
    }

    private boolean complies() {
        if (!npi.matches("[0-9]{10}")) {
            return false;
        }
        Character lastDigit = npi.charAt(9);
        List<Integer> everyOther = listWithEveryOtherDoubled(npi.substring(0, 9));
        int sum = 0;
        for (Integer num : everyOther) {
            sum += sumOfDigits(num);
        }
        int total = sum + 24; // 24 to account for 80840
        int units = total % 10;
        int checkDigit = (units != 0) ? (10 - units) : units;
        return (Character.getNumericValue(lastDigit) == checkDigit);

    }

    private List<Integer> listWithEveryOtherDoubled(String str) {
        List<Integer> nums = new ArrayList<Integer>();
        for (int i = 0; i < str.length(); i++) {
            if (i % 2 == 0) {
                nums.add(2 * Character.getNumericValue(str.charAt(i)));
            } else {
                nums.add(Character.getNumericValue(str.charAt(i)));
            }
        }
        return nums;
    }

    private static int sumOfDigits(int number) {
        int num = number;
        int sum = 0;
        while (num > 0) {
            sum += (num % 10);
            num = num / 10;
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println("Hello, World!");
        // System.out.println(sumOfDigits(16));
        System.out.println("1234567890".matches("[0-9]{10}"));
        System.out.println("123456789".matches("[0-9]{10}"));
    }

}