# npi-validator #

# Purpose #

* Used to check to see if a given National Provider Identification (NPI) is valid or not.

# Usage #
```
String npi = "1234567893";
if (new NpiValidator(npi).isValid()) {
    // yes - valid npi
} else {
    // no - invalid npi
}

```

# building #

* mvn -Dgpg.passphrase=<passphrase> clean package verify
* mvn -settings ~/.m2/ossrh-settings.xml clean package verify

# links #

* [How to calculate NPI check digit](https://www.eclaims.com/articles/how-to-calculate-the-npi-check-digit/)
* [npi validator algorithm](http://makdns.blogspot.com/2012/07/national-provider-identifier-npi.html)

# Keys #

## sending public keys to key servers ##

* [Instructions - Distributing your public key](https://central.sonatype.org/pages/working-with-pgp-signatures.html#distributing-your-public-key)
```
$ gpg2 --keyserver hkp://pool.sks-keyservers.net --send-keys 83F7FCBA45A3BC0C 
gpg: sending key 83F7FCBA45A3BC0C to hkp://pool.sks-keyservers.net
```
